# Déployer une app facilement from scratch

1. Créez vous un compte gitlab/github

2. Installer un app react sur votre machine
`npx create-react-app mon-app-de-test`

3. Sur gitlab/github, créez un nouveau repository vide et suivez les consignes pour push votre nouvelle app react

4. Une fois push, créez vous un compte netlify

5. Liez le a votre compte Gitlab/Github

6. Cliquez sur "New site from Git" et allez chercher votre app et votre branch de deploiement

7. Ajouter le yarn build au déploiement

8. Valider

Votre app est en ligne !

Vous pouvez changer ce que vous voulez dans le App.js et push sur master, votre code sera redéployer directement